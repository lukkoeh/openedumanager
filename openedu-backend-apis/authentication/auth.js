const express = require("express");
const uuid = require("uuid");
const mysql = require("mysql");
const sha256 = require("sha256");
const app = express();

const port = 8888;
const bind_ip = "0.0.0.0";

const pw_salt = "mysalt";
const hour_login_limit = 69;

const mysql_host = "localhost";
const mysql_user = "mysqluser";
const mysql_password = "securepassword";
const mysql_database = "mydatabase";

var db = mysql.createConnection({
  host: mysql_host,
  user: mysql_user,
  password: mysql_password
});

db.connect(function(err) {
  if (err) throw err;
  console.log("INFO: MySQL Login successful");
});

var sql_init = "USE ";
sql_init += mysql_database;
db.query(sql_init);

function saltandhash(password) {
  password += pw_salt;
  password = sha256(password);
  return password;
}

app.get("/login", function(req, res, next) {
    var user = req.headers.username;
    var pw = req.headers.password;
    var ip = req.connection.remoteAddress;
    pw = saltandhash(pw);

    db.query("SELECT COUNT(*) AS logins_last_hour FROM logins WHERE login_ip = ? AND valid = 0 AND login_time >= DATE_SUB(NOW(), INTERVAL 1 HOUR);", [ip], function(err, result){
      if (result[0].logins_last_hour >= hour_login_limit) {
        res.json({status_code: 429, message: "Too many login attempts", login: false, token: ""});
        return;
      } else {
        db.query("SELECT EXISTS(SELECT * FROM users WHERE username = ? AND password = ?) AS valid_credentials", [user,pw], function(err, result){
          if (result[0].valid_credentials == 1) {
            var generated_token = uuid.v4();
            db.query("INSERT INTO access_tokens(user, token, permanent, ip_addr) VALUES (?, ?, 0, ?)", [user, generated_token, ip], function(err, result) {
              db.query("INSERT INTO logins(login_user, login_ip, valid) VALUES (?, ?, 1)", [user, ip], function(err, result) {
                  res.json({status_code: 200, message: "Valid credentials, permission granted.", login: true, token: generated_token});
              });
            });
          } else {
            db.query("INSERT INTO logins(login_user, login_ip, valid) VALUES (?, ?, 0)", [user, ip], function(err, result) {
                res.json({status_code: 429, message: "Invalid credentials, permission denied.", login: false, token: ""});
            });
          }
        });
      }
    });
});

app.get("/register", function(req, res, next) {
    var user = req.headers.username;
    var pw = req.headers.password;
    var ip = req.connection.remoteAddress;
    var access_code = req.headers.access_code;
    
    pw = saltandhash(pw);
    
    db.query("SELECT COUNT(*) AS logins_last_hour FROM logins WHERE login_ip = ? AND valid = 0 AND login_time >= DATE_SUB(NOW(), INTERVAL 1 HOUR);", [ip], function(err, result) {
        if (result[0].logins_last_hour >= hour_login_limit) {
            res.json({status_code: 429, message: "Too many login/register attempts", register: false});
            return;
        } else {
            db.query("SELECT EXISTS(SELECT * FROM access_codes WHERE code = ? AND valid = 1) AS valid_code", [access_code], function(err, result) {
                if (result[0].valid_code == 1) {
                    db.query("INSERT INTO users(username, password, access_code) VALUES (?, ?, ?)", [user, pw, access_code], function(err, result) {
                        res.json({status_code: 200, message: "Valid access code, account created", register: true});
                    });
                } else {
                    res.json({status_code: 429, message: "Invalid access code, permission denied.", register: false});
                }
            });
        }
    });
});

app.listen(port, bind_ip);
